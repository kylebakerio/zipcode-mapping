
module.exports = {
	// importPaths: {
	//  // NOTE: this is here for reference, but really this should be stored in pm2config.json. That is the source of truth for this.
	// 	cloud: 'https://storage.googleapis.com/regional-base-finetype-bucket/attempt5_lcoCP4_simp001_clean.json',
	// 	f1micro: "../zipcodes.json",
	// 	T460: "./zcta_renderings/gmaps_compatible_renderings/attempt5_lcoCP4_simp001_clean.json",
	// },
	importThis: {
		// supply shapefile -> JSON -> mapgeo.sh -> output here (in lieu of DB)
		// see mapgeo.sh for more info
		simpleRequire(src) {
			// note: require() is a synchronous function. It requires lots of ram and blocks execution thread. But that's fine for local development,
			// and is honestly reasonably performant, only paying this price once at startup to load into memory.
			// original way was: var zipcodeDict = require('../react-spa-starter/src/static/prettyjson/zcta5-simple-p001-lco-4-pretty-ws-ns2.json');
			const start = Date.now();
			return new Promise((resolve, reject) => {
				resolve(require(src));
				console.log(`completed simpleRequire, took ${(Date.now() - start) / 1000}~ seconds`);
			});
		},
		cloudRequestEval(src) {
			// implemented for deploying to ZEIT now, to get around need for 1mb file size requirements.
			// tested locally, but decided not to deploy--probably would have suffered similar ram problems
			// as f1micro did.
			// !! note: since refactoring, it hasn't worked yet. But it did pre-refactor.
			// tweak as needed before using, but
			// note: this can definitely crash the system.
			// note: should probably integrate the readstream method below.
			// importing from google cloud storage using request library:
			const request = require('request');
			return new Promise((resolve, reject) => {
				const start = Date.now();
				request(src, (error, response, body) => {
					console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					
					if (!error) {
						console.log("welp, no error:", body.length);
						console.log(`finished cloud request in ${(Date.now() - start) / 1000}~ seconds, though now we need to parse response....`);
						// eval(`const zipcodeDict = ${body}`);
						resolve(eval("(body)"));
					}
					else console.log("error:", error);
				});
			})
		},
		customReadStream(src) {
			// stream method for importing, extremely light on memory requirements.
			// designed _only_ to work for the output of mapgeo.sh in this repo,
			// and used only for allowing high enough performance to run on lightweight f1 micro Google Cloud Compute server.
			// expects the document to look _exactly_ like this:
			// {
			//   "00400": { [geoJSON data for 00400 here] },
			//   "02000": { [geoJSON data for 02000 here] },
			//   "00010": { [geoJSON data for 00010 here] }
			// }
			// with every line being a single zip code's geoJSON data, other than the first and list line
			// NOTE: if mapgeo.sh is updated to have different output, this is likely to break.
			// In that case, consider updating to a proper JSON streaming library.
			// (e.g.: http://oboejs.com/examples#loading-json-trees-larger-than-the-available-ram)
			return new Promise((resolve, reject) => {
				var readline = require('readline');
				var fs = require('fs');

				let lineCount = 0;
				const zipcodeDict = {};
				const start = Date.now();
				readline.createInterface({
				  input: fs.createReadStream(src),
				  terminal: false,
				})
				.on('line', function(line) {
				  lineCount++;
				  
				  if (line.length > 3) {
				  	try {
				  		zipcodeDict[line.slice(3,8)] = JSON.parse(line.slice(10,line.length-1));
				  	}
				  	catch (e) {
				  		try {
				  			console.warn("retrying for line", line.slice(3,8));
				  			zipcodeDict[line.slice(3,8)] = JSON.parse(line.slice(10,line.length));
				  			console.log("success!");
				  		}
				  		catch(e) {
					  		console.error("\n\nIMPORT ERROR\n\n",e,line);
				  		}
				  	}
				  }
				  else if (line === "}") {
				    console.log(`\n finished readline import in ${(Date.now() - start) / 1000}~ seconds`);
				    resolve(zipcodeDict);
				  }
				});
			})
		},
		asyncJson(src) {
			// async, but not streaming. Doesn't block execution thread, but still uses a lot of memory.
			var fs = require('fs');
			const start = Date.now();
			return new Promise((resolve, reject) => {
				fs.readFile(src, 'utf8', function (err, data) {
				    if (err) throw err;
				    console.log(`\n finished asyncJson import in ${(Date.now() - start) / 1000}~ seconds`);
				    resolve(JSON.parse(data));
				});
			});
		},
	},
};
