# Deployed
  [zipcoder.kylebaker.io](http://zipcoder.kylebaker.io)

## About
  Read the detailed [blog post](http://code.blog.kylebaker.io/2018/04/02/mapping-zipcodes/).

## Included

Included are:
  - Express server with endpoints for:
    - creating new users
    - logging in
    - getting zipcode coordinates
  - HTML/CSS doc. It's centered with flexbox, and has a relatively responsive design. :)
  - map-my-zipcode.js, required by HTML doc to do anything interesting
  - bash/node script for converting output of ogr2ogr into a format that works well for google maps API v3 in general, and for this intended client in particular.

## Not Included

zipcode GeoJSON data or shapefile. Too big for this repo. Instructions for getting that are found in `pre-developing.md`.

## Features

  - Asks for username, then
    - retrieves saved data for that user if it exists
    - creates a new user if that user does not exist
  - loads up a region, and a dropdown with that user's regions; 
  - if there are none, it waits until a user clicks on the map or 'create a region' buttons;
  - if regions with zipcodes were loaded, it pans the map to a zipcode of an active region.
  - if the 'save' button is clicked, the region is saved to the server, and available upon page refresh.
  - when delivering user profile on initial load, coordinates are loaded up for all relevant zip codes so that later pings to the server are not necessary for those zip codes
  - all zipcode coordinates retrieved from server are cached client-side for further use
  - any zipcode can be toggled on/off for that region by clicking it
  - given that google's click response for a zipcode cannot be expected to perfectly line up with our polygon shapes, we handle clicks for adjacent zipcodes that google 'misidentifies' as an existing zipcode by dropping the click, instead of accidentally double-highlighting that polygon or disabling that polygon. We also put up a text box explaining that zip is already added.
  - shell script has a lot of superfluous bits, because it went through many iterations, and not all features I played with for it are currently working, but with just a bit they could be. This includes various logging options(time taken, coordinate density, and # of zipcodes were all useful for debugging and understanding ogr2ogr's tool), and error handling being silent (maybe you would want to swallow two errors for edge cases rather than have that just kill the script, if it came to it), formatting (readable output, or compressed output?), and speed (matters for the full size file, which turns that >500mb file into an up to 1.8gb geojson file). It's also pretty hacky--if I were doing this right, I'd have used something like commander to properly support bash options and documentation, etc.
  - server provides a versioned API. :)

## Features Added Later

   - larger buttons and dropdown
   - functionality to switch users without refreshing
   - style shifter and custom map styles
   - live deployment
   - pm2 config setup
   - streaming JSON import for server
   - extending original textbox to also allow moving the map to any location
   - upgrade: instead of just moving the map to _a_ zipcode border coordinate of selected region, now moves view to contain whole region.
   - pressing enter key in text box behaves as one would expect
   - info bubbles are more useful
   - zipcoder.js got some much needed refactoring
   - explanatory hover text on interface buttons
