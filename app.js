const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const debug = true;
const environment = process.env.NODE_ENV;
const port = process.env.PORT || 3003;
const { importThis } = require('./zipcode-import.js');
const zipcodeSrcPath = process.env.ZIPCODES_SRC;
const way = process.env.IMPORT_METHOD;
console.log(`environment: ${environment}, port: ${port}`);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true,
})); 

console.log(`importing and building zipcode library at\n${zipcodeSrcPath}\n using\n${way}()\nmethod...`);

let zipcodeDict = {};

const buildZipcodeDict = importThis[way](zipcodeSrcPath).then(dict => {
	console.log("Imported this many zipcodes:", Object.keys(dict).length);
	Object.assign(zipcodeDict, dict);
});

//
// end import stuff.
//

const authenticated = true; // :D

// const getZipcode = function getZipcode(zipcode) {
// 	return new Promise((resolve, reject) => {
// 		buildZipcodeDict.then(() => resolve(zipcodeDict[zip]));
// 	});
// };

const getZipcode = async function getZipcode(zipcode) {
	console.log(`awaiting for ${zipcode}`)
	await buildZipcodeDict;
	// console.log(`it's built now, so I'll return for ${zipcode}:`);
	// console.log(`${JSON.stringify(zipcodeDict[zipcode], null, 2)}`);
	return zipcodeDict[zipcode];
};

app.get('/v1/userdata/:username', async (req, res) => {
	console.log(`attempting login as ${req.params.username}`);

	const username = req.params.username;

	if (authenticated && userProfileDB.hasOwnProperty(username)) {
		const userData = userProfileDB[req.params.username];
		const coords = {};

		await buildZipcodeDict;
		Object.keys(userData.Regions).forEach(regionName => {
			Object.keys(userData.Regions[regionName]).forEach(async zip => {
				// coords[zip] = zipcodeDict[zip];
				coords[zip] = zipcodeDict[zip];
			})
		})

		const responseBody = JSON.stringify({
			userData,
			coords,
			status: 'success',
			error: false,
		});

		res.send(responseBody);
	}
	else if (authenticated && !userProfileDB.hasOwnProperty(username)) {
		res.send({
			msg: 'user doesn\'t exist, please forward request to route provided.',
			route: '/v1/create-user/{username}',
			status: 'redirect',
			err: true,
		});
	}
})

app.post('/v1/userdata/:username', function(req, res) {
	if (authenticated) {
		if (req.body.hasOwnProperty('Regions')) {
			logger('request', req.body)
			Object.keys(req.body.Regions).forEach(region => {
				userProfileDB[req.params.username].Regions[region] = req.body.Regions[region];
			});
			logger('updated to:', userProfileDB);
			res.send({
				status: 'success',
				msg: 'updated user data',
				userData: debug ? userProfileDB[req.params.username] : '' });
		}
		else {
			console.log("could not update user info for:", JSON.stringify(req.body, null, 2))
			res.send({msg: 'updating that value is not yet supported', status: 'fail'});
		}
	}
})

app.post('/v1/create-user/:username', function(req, res) {
	const username = req.params.username;
	// logger("huh?", req.req.params, Object.keys(req))
	// console.log("attempting to create user:", req.req.params.username)
	if (!userProfileDB.hasOwnProperty(username)) {
		userProfileDB[username] = { Regions: {} };
		res.send({
			error: false,
			status: "success",
			userData: userProfileDB[username]
		});
	}
	else {
		// was res.res.send, pretty sure that was an error, but just in case you run into trouble here.
		res.send({
			error: true,
			status: 'fail',
			msg: `username ${username} taken`
		});
	}
});

app.get('/v1/zipcode/:zipcode', function (req, res) {
	console.log("got a request for zipcode " + req.params.zipcode);
	const zipcode = Number(req.params.zipcode);

	if (isNaN(zipcode) || req.params.zipcode.length !== 5) {
		res.send({
			err: true,
			status: "fail",
			msg:'invalid request! Should be a 5 digit number. Instead, got: ' + req.params.zipcode,
		});
	}
	else if (!zipcodeDict.hasOwnProperty(zipcode)) {
		res.send({
			err: true,
			status: "fail",
			msg: `invalid request, no such zipcode ${req.params.zipcode}. Note that some USPS zipcodes are not present in the census data used here.`,
		});
	}
	else {
		const GeoJSON = zipcodeDict[zipcode];
		console.log("sending", GeoJSON)
		res.send({
			err: false,
			status: "success",
			msg: 'you requested ' + req.params.zipcode,
			GeoJSON,
		});
	}
});

app.listen({
	host: '0.0.0.0',
	port: port,
	exclusive: true
}, function () {
	console.log(`Let\'s get some zipcodes on ${port}!`);
});

app.use('/', express.static(__dirname));

//
// add DB here. ;)
//
const userProfileDB = {
	guest: {
		Regions: {
			"Austin NW": {
				78613: 1,
				78726: 1,
				78750: 1
			},
			"Austin NE": {
				78653: 1,
				78660: 1,
				78664: 1,
				78724: 1
			}
		},
	},
}

const logger = function logger(msg, val) {
	console.log(msg, JSON.stringify(val, null, 2));
}
