#!/usr/bin/node

const fs = require('fs');
const readline = require('readline');

const inFile = process.argv[2]; // just a filename, should be the output of ogr2ogr run on the zcta data?
const outFile = process.argv[3]; // just a filename...
const fast = process.argv[4] === '-fast'; // currently deprecated
const failfast = process.argv[4] === '-fail'; // when using large files, can be valuable to force it to abort vs. just logging it and carrying on

const start = Date.now(); // for logging time taken to execute
const writableStream = fs.createWriteStream(outFile);

let finished = false;
let lineCount = 0;
let shown = false;
let zipcodeCount = 0;
let coordCount = 0;
let errorLog = [];


const mapOneNew = function mapOneNew(geoJsonSingleLine) {
  // ogr2ogr gives coordinate sets that can be in a 'superset' form that google won't recognize.
  // so we turn each one into its own feature, which then go together in a FeatureCollection.
  // note: this function relies on the output of ogr2ogr being JSON with an object per line.
  zipcodeCount++;

  if (geoJsonSingleLine[geoJsonSingleLine.length - 1] === ',') { // if last character is a comma, then we're not on last entry
    geoJsonSingleLine = geoJsonSingleLine.slice(0, geoJsonSingleLine.length - 1); // turn it into a valid object by removing comma from the line
  }
  else {
    // last line, set flag for stream to finish outside
    finished = true;
  }

  const feature = JSON.parse(geoJsonSingleLine); // the line is a feature
  const zipcode = feature.properties.GEOID10; // our key will be the zipcode, which we find here either from the zcta_510 source data

  const featureArray = feature.geometry.coordinates.map((onePolygon, index) => {
    coordCount += onePolygon.length;

    let coordinates;

    if (typeof onePolygon[0][0] === 'number') {
      coordinates = [onePolygon];
    }
    else if (typeof onePolygon[0][0][0] === 'number') {
      coordinates = onePolygon;
    }
    else {
      if (failfast) throw new Error("Polygon depth problem on " + zipcode);
      else errorLog.push("Polygon depth problem on " + zipcode);
      coordinates = onePolygon;
    }

    // what individual features look like
    return {
      type: 'Feature',
      id: zipcode,
      properties: {
        zipcode,
        title: zipcode,
        part: index,
        groupOf: feature.geometry.coordinates.length,
        strokeColor: '#0048ff',
        strokeOpacity: 0.5,
        strokeWeight: 2,
        fillColor: '#0048ff',
        fillOpacity: 0.35,
      },
      geometry: {
        type: 'Polygon',
        coordinates,
      }
    }
  });

  // format Google is able to handle groups of Features
  const featureCollection = JSON.stringify({
    type: 'FeatureCollection',
    features: featureArray,
  });

  // output is a string of JSON for a file. includes a comma, unless last line.
  return `  "${zipcode}": ${featureCollection}${!finished ? ',' : ''}\n`;
};


const mapper = mapOneNew; // fast ? mapOneFast : mapOne;

writableStream.write("{\n"); // open the output file's JSON object

readline.createInterface({
  input: fs.createReadStream(inFile),
  terminal: false,
})
.on('line', function(line) {
  lineCount++;
  
  if (lineCount > 3 && !finished) { // first few lines aren't the guts we need to process, skip them
    writableStream.write(mapper(line));
  }
  else if (finished && !shown) {
    console.log(`input was ${inFile}, output was ${outFile}`);
    console.log(`done, wrote ${zipcodeCount} zipcodes and ${coordCount} coordinates in ${(Date.now() - start) / 1000}~ seconds`);
    
    if (errorLog.length) console.log(errorLog);
    
    shown = true;
    writableStream.write('}\n'); // close the output file's JSON object
  }
});
