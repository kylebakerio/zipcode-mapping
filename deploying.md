merge in:
  `git pull`
push changeS:
  `git push`
login:
  `gcloud compute ssh instance-1`
go to folder:
  `cd code/zipcode-mapping/`
merge local changes:
  `git pull`
run package updates that may have been comitted:
  `yarn install`
restart process: 
  `npx pm2 stop zipcoder`
  `npx pm2 start pm2config.json --env f1micro`
monitor process:
  `npx pm2 logs`
end ssh connection:
  `exit`

other useful commands:
  `npx pm2 stop all`
  `npx pm2 monit zipcoder`
  `npx pm2 show zipcoder`

_note:_
fimicro deploy presumes that it will have access to the zipcodes list
according to settings in pm2config.json; see `pre-developing.md`
