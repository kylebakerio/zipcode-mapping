developing:

(first, make sure you have zipcodes in GeoJSON format--check pre-developing.md.)

`npx pm2 start pm2config.json --watch --env T460`
(replace "T460" with your environment, as added to pm2config.json, e.g "T460" for "environment_T460".)

`npx` allows you to run pm2 without installing it globally--that means we can be sure the version of 
pm2 used in development is the verison we're using in production.

"--watch" forces auto-reload of server on file save.
"--env T460" lets me develop with my known local needs. (It sets custom filepaths that are
not include in the repo, and that may change for every user.)

If contributing, add your own environment variables there locally, and then .gitignore the file.
You should merge updates to it if you're going to deploy, and you may consider merging
updates to it if things stop working for some reason.
