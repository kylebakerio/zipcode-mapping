## Generating Zipcode Coordinates

1. run `yarn install` in this repo
2. you'll need to get a shapefile of zipcodes. I'm using zcta_510, US census data, for using this in the US. It contains a shapeful containing the coordinates of every US zipcode. (try here: https://www.census.gov/geo/maps-data/data/cbf/cbf_zcta.html). Problem: USPS zipcodes don't perfectly match Census zipcodes. Getting USPS geographical data is difficult, because they aren't defined geographically, they change regularly as needed, etc. Some have tried to generate this data anyways--it varies according to their methedology, and they usually charge a good amount for it.
3. you'll need ogr2ogr, which comes from the gdal package. See install instructions here: https://trac.osgeo.org/gdal/wiki/DownloadingGdalBinaries
4. you'll then use ogr2ogr to convert that shapefile to JSON. Example: 
(give it a bit to run, it can take a while to chew a 800+mb file into your new file that will be between 120mb and 1.8gb depending on selected precision.)

`ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=1 zcta_renderings/output_a1_ogr_lcoCP1.json tl_2014_us_zcta510/tl_2014_us_zcta510.shp`

gives me a 1GB file... want smaller? Let's try adding the simplify flag:

`ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=1 -simplify 0.0001 zcta_renderings/output_a2_ogr_lcoCP1_simp0001.json tl_2014_us_zcta510/tl_2014_us_zcta510.shp`

207mb, but generates squares and lines for zipcode shapes. That won't work...

`ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=4 -simplify 0.001 zcta_renderings/output_a3_ogr_lcoCP4_simp001.json tl_2014_us_zcta510/tl_2014_us_zcta510.shp`

That's 70mb, but usable shapes. Good starting point.

`ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=4 -simplify 0.0001 zcta_renderings/a6_lcoCP4_simp0001_ogr.json zcta/tl_2014_us_zcta510/tl_2014_us_zcta510.shp`

This one is just over 200mb. Node.js will load this fine into memory, but for all your extra memory footprint, you don't get much. The shapes look almost identical. That said, it is a little nicer looking--not the curved of the underlying street will line up reasonably well with the zipcode polygons, for example. There are far fewer examples of gaps between borders of zipcode boundaries, which is nice. But the difference is subtle.

5. clean up the output of ogr2ogr by running it through _mapgeo.sh_, since google maps doesn't fully support every possible structure of the GeoJSON spec, it seems. So:

Make it executable:
`chmod +x mapgeo.sh`

Run the file through:
`./mapgeo.sh zcta_renderings/a6_lcoCP4_simp0001_ogr.json zcta_renderings/gmaps_compatible_renderings/new_output_3_ogr_clean.json`

6. now you're ready to switch over to developing.md to continue.
