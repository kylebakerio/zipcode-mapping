let map;
let username;
let userData = { Regions: {} };
let zipcodeDict = {};
let activeRegion;
let activeLayer = {};
let infowindow;
let waitingForStart = true;

//
// User Management Methods
//

const login = async function login(checkKey=false) {
  if (checkKey && event.keyCode != 13) return; // for pressing enter in textbox.
  username = $('#username-input').val();
  if (!username) {
    alert("Please select a username if you want to login.");
    return;
  }

  console.log('login()', checkKey, event.keyCode === 13)
  logOut();

  userData = {};
  return $.ajax({
    url:'/v1/userdata/' + username,
    method: 'GET',
    dataType: 'json',
    success: function(data) {
      if (data.status === 'success') {
        loadUserData(data);
      }
      else if (data.status === 'redirect') {
        makeNewUser(username, data.route);
      }
    },
    error: notifyError,
  });
};

const logOut = function logOut() {
  if (userData && userData.Regions && userData.Regions[activeRegion]) {
    Object.keys(userData.Regions[activeRegion]).forEach(zipcode => unhighlightZipcode(zipcode))
  }
  $("select").empty();
  userData = { Regions: {} };
  activeRegion = "";
  let activeLayer = {};
};

const loadUserData = function loadUserData(data) {
  console.log("loadUserData()")    
  userData = data.userData;
  const regions = userData.Regions;
  const coords = data.coords;

  zipcodeDict = Object.assign(zipcodeDict, coords);

  Object.keys(regions).forEach(regionName => {
    const attrs = { text: regionName };
    const region = regions[regionName];
    if (!activeRegion) {
      selectRegion(regionName);
      attrs.selected = 'selected';
    }
    $('select').append($('<option>', attrs));
  })
  waitingForStart = false;
};

const makeNewUser = function makeNewUser(username, route) {
  console.log('makeNewUser()')
  $.ajax({
    url:'/v1/create-user/' + username,
    method: 'POST',
    dataType: 'json',
    success: function(data) {
      if (data.status === 'success') {
        loadUserData(data);
      }
      else if (data.status === 'fail' && data.msg === 'username taken') {
        // can't hit this condition, of course, since a username is always a login right now.
        username = prompt("Sorry, that username appears to be taken. Try another?")
        login();
      }
    },
    error: (er) => { throw new Error(er) },
  });
};

//
// GeoJSON API
//

const fetchZipcodeCoords = function fetchZipcodeCoords(zipcode) {
  console.log('fetchZipCoords()')
  return $.ajax({
    url:'/v1/zipcode/' + zipcode,
    method: 'GET',
    dataType: 'json',
  });
};


//
// HTML event interface
//

// When using search box
window.addZipcodeViaTextbox = function addZipcodeViaTextbox(checkKey=false) {
  if (waitingForStart || checkKey && event.keyCode != 13) return; // for pressing enter in textbox.
  console.log("search box click")
  
  let input = $('#add-zipcode-input').val();
  if (isNaN(Number(input))) {
    // If they type something other than a zipcode, let's just go there.
    panToLocation(input);
  }
  else {
    // if it's a zipcode, add it and go there.
    zipcode = input;
    addZipcodeToActiveRegion(zipcode)
    .then(panToZipcode)
    // .then(() => alert("Added " + zipcode))
    .then(() => addInfoBubble(zipcode, event || false))
    .catch(notifyError)
  }
};

//
// Movement Helpers
//

const zipcodeToLatLng = function zipcodeToLatLng(zipcode) {
  // return one point from border
  console.log(`zipcodeToLatLng(${zipcode})`);
  return {
    lng: zipcodeDict[zipcode].features[0].geometry.coordinates[0][0][0],
    lat: zipcodeDict[zipcode].features[0].geometry.coordinates[0][0][1]
  };
};

const zipcodeToLatLngs = function zipcodeToLatLngs(zipcode) {
  // return all points of border
  console.log(`zipcodeToLatLngs(${zipcode})`);
  let output = [];
  zipcodeDict[zipcode].features.forEach(feature => {
    output.push(...feature.geometry.coordinates[0].map(coord => ({
      lng: coord[0],
      lat: coord[1]
    }) // inner anon function()
    ) // map()
    ) // push()
  }); // forEach()

  return output;
};

const panToZipcode = function panToZipcode(zipcode) {
  map.panTo(zipcodeToLatLng(zipcode));
};

// pick either argument, null first argument if you want second
const mapToBounds = function mapToBounds(arrayOfLatLngs, boundsObject=null) {
  console.log(`mapToBounds()`, arguments)
  const bounds = boundsObject || new google.maps.LatLngBounds();
  if (arrayOfLatLngs) {
    arrayOfLatLngs.forEach(latLng => bounds.extend(latLng))
  }
  map.fitBounds(bounds);
  // map.setZoom(map.getZoom()-1);
};

//
// Info Bubble Utilities
//

const genericInfoBubble = function genericInfoBubble(latLng, message) {
  console.log(`genericInfoBubble(${latLng}, ${message})`);
  let bubbleNotification = `
  <table>
    <tr><td>${message}</td></tr>
  </table>`;
  infowindow.setContent(bubbleNotification);
  infowindow.setPosition(latLng);
  infowindow.open(map);
};

const addInfoBubble = function addInfoBubble(zipcode, event=false) {
  console.log(`addInfoBubble(${zipcode}, ${event})`, event.latLng);
  let bubbleNotification = `
  <table>
    <tr><td>Added</td><td>${zipcode}</td></tr>
  </table>`;
  infowindow.setContent(bubbleNotification);
  infowindow.setPosition(event && event.latLng ? event.latLng : zipcodeToLatLng(zipcode));
  infowindow.open(map);
};

//
// Zipcode Highlighters
//

const unhighlightZipcode = function unhighlightZipcode(zipcode) {
  console.log(`unhighlightZipcode(${zipcode})`);
  activeLayer[zipcode].forEach(feature => geoJsonLayer.remove(feature));
  delete activeLayer[zipcode];
};

const highlightZipcode = function highlightZipcode(zipcode) {
  console.log(`highlightZipcode(${zipcode})`);
  userData.Regions[activeRegion][zipcode] = 1;
  const features = geoJsonLayer.addGeoJson(zipcodeDict[zipcode]);
  activeLayer[zipcode] = features;
  return zipcode;
};

//
// Add/Remove Zipcode to Region
// 

const addZipcodeToActiveRegion = function addZipcodeToActiveRegion(zipcode) {
  return new Promise((resolve, reject) => {
    console.log(`addZipcodeToActiveRegion(${zipcode}})`);
    if (activeLayer.hasOwnProperty(zipcode)) {
      // ignore; this usually happens when the polygons don't line up perfectly,
      // and a click outside the polygons for a zip, but the geocoder says
      // it was in that polygon's zip.
      //  This error can be due to a mismatch between Google's data and census data,
      // as well as due to impreceision in polygons.
      genericInfoBubble(zipcodeToLatLng(zipcode), `Already added ${zipcode}`);
      reject(`That zipcode, ${zipcode}, is already added.`);
    }
    else if (zipcodeDict.hasOwnProperty(zipcode)) {
      // when we've already hit the API in this session for the coordinates.
      highlightZipcode(zipcode);
      resolve(zipcode);
    }
    else {
      // get GeoJSON for zipcode
      if (!activeRegion) {
        createRegion();
      }

      fetchZipcodeCoords(zipcode)
      .then(data => {
        console.log("fetched:", data)
        if (data.status === "fail") {
          reject(data)
        }
        else {
          zipcodeDict[zipcode] = data.GeoJSON;
          highlightZipcode(zipcode);
          resolve(zipcode);
        }
      })
      .fail(notifyError)
    }
  });
};

const removeZipcodeFromActiveRegion = function removeZipcodeFromActiveRegion(zipcode) {
  console.log("removeZipcodeFromActiveRegion()")
  delete userData.Regions[activeRegion][zipcode];
};

//
// Region Handlers
//

const saveRegion = function saveRegion() {
  if (waitingForStart) return;
  console.log("saveRegion()")
  const currentRegion = $("#region-select :selected").text();
  console.log(currentRegion);
  const data = { Regions: {} };
  data.Regions[currentRegion] = userData.Regions[currentRegion];
  $.ajax({
    url:'/v1/userdata/' + username,
    method: 'POST',
    dataType: 'json',
    data,
    success: (data) => {
      console.log('saving new fields:', data.msg);
    },
    error: (er) => { throw new Error(er) },
  });
};

const createRegion = function createRegion() {
  if (waitingForStart) return;
  console.log("createRegion()")
  newRegionName = prompt("What would you like to name this region?", `Region ${Object.keys(userData.Regions).length + 1}`);
  if (newRegionName.length < 1 || Object.keys(userData.Regions).includes(newRegionName)) {
    alert("Sorry, you need a new region name to continue.");
    return
  }
  $('select').append($('<option>', { text: newRegionName, selected: 'selected' }));
  userData.Regions[newRegionName] = {};
  selectRegion(newRegionName);
};

const selectRegion = function selectRegion(regionName) {
  console.log("selectRegion()", regionName);
  const region = userData.Regions[regionName];
  
  if (activeRegion) {
    Object.keys(userData.Regions[activeRegion]).forEach(zipcode => {
      unhighlightZipcode(zipcode);
    })
  }

  activeRegion = regionName;

  const allRegionBoundaryPoints = [];
  var bounds = new google.maps.LatLngBounds();
  Object.keys(region).forEach(zipcode => {
    addZipcodeToActiveRegion(zipcode);
    allRegionBoundaryPoints.push(...zipcodeToLatLngs(zipcode));
  })
  if (allRegionBoundaryPoints.length) mapToBounds(allRegionBoundaryPoints);
};


//
// Maps API Initialization
//

const initMap = function initMap() {
  console.log("initMap()")
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: {
      lat: 30.4668,
      lng: -97.8044,
    },
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  map.setOptions({
    styles: window.mapStyles.minimalMapStyle // default, see map-styles.js,
  });

  const geocoder = new google.maps.Geocoder();

  $('select').on('change', function onSelectChange(){
    selectRegion(this.value)
  });


  // listener to getZipCode 
  map.addListener('click', function(event) {
    console.log("map click")
    console.log('active region:', activeRegion)
    if (!username) {
      alert("Please click 'Switch To User' to begin.")
    }
    else if (!activeRegion) {
      createRegion();
    }
    if (activeRegion) {
      // refactor to promise
      reverseGeocodeClick(event);
    }
  });

  map.data.addListener('click', function(event) {
    // console.log('map.data.click event:', event.latLng, event.latLng())
  });

  // Load geoJSON
  geoJsonLayer = new google.maps.Data();
  geoJsonLayer.setMap(map);

  // Set click event for infowindow
  infowindow = new google.maps.InfoWindow();
  geoJsonLayer.addListener('click', function(event) {
    console.log("geojson click")
    const zipcode = event.feature.getProperty('zipcode');
    removeZipcodeFromActiveRegion(zipcode);
    unhighlightZipcode(zipcode);

    let bubbleNotification = `
    <table>
      <tr><td>Removed</td><td>${zipcode}</td></tr>
    </table>`;
    infowindow.setContent(bubbleNotification);
    infowindow.setPosition(event.latLng);
    infowindow.open(map);
  });

  window.panToLocation = function panToLocation(location, geocodeRequest) {
    // pick either location, or null + a direct geocodeRequest
    // https://developers.google.com/maps/documentation/javascript/geocoding
    // {
    //   address: string,
    //   location: LatLng,
    //   placeId: string,
    //   bounds: LatLngBounds,
    //   componentRestrictions: GeocoderComponentRestrictions,
    //   region: string
    // }

    // var address = document.getElementById('address').value;
    geocoder.geocode(
      location ? { "address": location } : geocodeRequest,
      // { placeId },
      function(results, status) {
        if (status == 'OK') {
          formattedLocation = results[0].formatted_address;
          mapToBounds(null, results[0].geometry.viewport)

          genericInfoBubble(results[0].geometry.location, `Welcome to ${formattedLocation}`);

          // var marker = new google.maps.Marker({
          //     map: map,
          //     position: results[0].geometry.location
          // });
        }
        else {
          notifyError('Geocode was not successful for the following reason: ' + status);
          console.error('Geocode:', status);
        }
        console.warn('geocode results:', results)
      }
    );
  }

  // get zipcode on map click
  const reverseGeocodeClick = function reverseGeocodeClick(event) {
    console.log("reverseGeocodeClick()")
    const lat = event.latLng.lat();
    const lng = event.latLng.lng();
    const latLng = new google.maps.LatLng(lat, lng);

    geocoder.geocode({ latLng }, (results, status) => {
      try {
        if (status === google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            for (let j = 0; j < results[0].address_components.length; j++) {
              if (results[0].address_components[j].types[0] === 'postal_code') {
                const zipcode = results[0].address_components[j].short_name
                addZipcodeToActiveRegion(zipcode)
                .then(() => addInfoBubble(zipcode))
                break;
              }
            }
          }
          else {
            throw new Error("response appears to be an empty array. :(")
          }
        }
        else {
          throw new Error("Geocoder failed due to: " + status);
        }
      }
      catch(e) {
        notifyError(e);
      }
    });
  };
};

google.maps.event.addDomListener(window, 'load', initMap);

//
// Error helpers
//

const notifyError = function notifyError(err) {
  console.error(`error: ${err}`);
  console.error(JSON.stringify(err))
  alert(err.msg || err);
};

//
// old snippet for future reference from original draft:
//
    // Search in the layer and trigger an event
    // geoJsonLayer.forEach(function(feature) {
    //   if (searchString === feature.getProperty('letter')) {
    //     // Zoom to this feature and pop its infobox
    //     let foundFeatureGeo = feature.getGeometry().getAt(0).getArray();
    //     let bounds = new google.maps.LatLngBounds();
    //     for (let i = 0; i < foundFeatureGeo.length; i++) {
    //       bounds.extend(foundFeatureGeo[i]);
    //     }
    //     map.fitBounds(bounds);
    //     google.maps.event.trigger(geoJsonLayer, 'click', {
    //       // arbirarily pick the first point of the outer ring of the polygon
    //       latLng: foundFeatureGeo[0],
    //       "feature": feature
    //     });
    //   }
    // });
    // map.data.overrideStyle(event.feature, {fillColor: 'red'});
